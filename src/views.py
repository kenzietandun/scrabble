#!/usr/bin/env python3

import PySimpleGUI as sg


def get_main_window_layout(initial_tiles, board_bonus):
    tile_size = (2, 1)
    board_size = 15

    def get_bgcolor(location):
        triple_word_color = "#b71c1c"
        double_word_color = "#fb8c00"
        double_letter_color = "#b2ff59"
        triple_letter_color = "#26c6da"
        board_default_color = "#efefef"

        if location not in board_bonus:
            return board_default_color
        elif board_bonus[location] == "TW":
            return triple_word_color
        elif board_bonus[location] == "DW":
            return double_word_color
        elif board_bonus[location] == "TL":
            return triple_letter_color
        elif board_bonus[location] == "DL":
            return double_letter_color

    board_layout = [[sg.Text(i, size=(2, 1)) for i in range(0, 16)]]

    board_layout.extend(
        [
            [
                sg.InputText(
                    "",
                    background_color=get_bgcolor((x + 1, y + 1)),
                    text_color="#000000",
                    size=tile_size,
                    key=(x + 1, y + 1),
                    justification="center",
                    enable_events=True,
                )
                for x in range(board_size)
            ]
            for y in range(board_size)
        ]
    )

    for y in range(1, 16):
        board_layout[y].insert(0, sg.Text(y, size=(2, 1)))

    player_tiles_layout = [
        [
            sg.Button(initial_tiles[0], key="t1", disabled=False, size=(3, 1)),
            sg.Button(initial_tiles[1], key="t2", disabled=False, size=(3, 1)),
            sg.Button(initial_tiles[2], key="t3", disabled=False, size=(3, 1)),
            sg.Button(initial_tiles[3], key="t4", disabled=False, size=(3, 1)),
            sg.Button(initial_tiles[4], key="t5", disabled=False, size=(3, 1)),
            sg.Button(initial_tiles[5], key="t6", disabled=False, size=(3, 1)),
            sg.Button(initial_tiles[6], key="t7", disabled=False, size=(3, 1)),
        ]
    ]

    game_layout = [
        [sg.Frame("Board", layout=board_layout)],
        [sg.Frame("Tiles", layout=player_tiles_layout),],
    ]

    scores_layout = [
        [
            sg.Text("", key="_player0_name", size=(15, 1)),
            sg.Text("", key="_player0_score", size=(3, 1)),
        ],
        [
            sg.Text("", key="_player1_name", size=(15, 1)),
            sg.Text("", key="_player1_score", size=(3, 1)),
        ],
        [
            sg.Text("", key="_player2_name", size=(15, 1)),
            sg.Text("", key="_player2_score", size=(3, 1)),
        ],
        [
            sg.Text("", key="_player3_name", size=(15, 1)),
            sg.Text("", key="_player3_score", size=(3, 1)),
        ],
    ]

    action_buttons_layout = [
        [sg.Button("Exchange", size=(17, 1))],
        [sg.Button("Undo", size=(17, 1))],
        [sg.Button("Shuffle", size=(17, 1))],
        [sg.Button("Predict score", size=(17, 1)),],
        [sg.Text("Predicted:"), sg.Text("", size=(3, 1), key="_score_text")],
        [sg.Text("")],
    ]

    turn_info_layout = [[sg.Text("", size=(19, 1), key="_turn_info")]]

    actions_layout = [
        [sg.Frame("Turn", layout=turn_info_layout)],
        [sg.Frame("Scores", layout=scores_layout)],
        [sg.Frame("Actions", layout=action_buttons_layout)],
        [sg.Button("Place", size=(20, 2)),],
    ]

    return [[sg.Column(game_layout), sg.Column(actions_layout)]]


def get_setup_window_layout():
    mode_layout = [
        [
            sg.Radio(
                "Create game",
                "mode",
                size=(21, 1),
                key="_create",
                enable_events=True,
                default=True,
            )
        ],
        [sg.Radio("Join game", "mode", size=(21, 1), key="_join", enable_events=True)],
    ]

    create_game_layout = [
        [
            sg.Text("Room", size=(10, 1)),
            sg.InputText("", key="_room_name", size=(12, 1)),
        ],
        [
            sg.Text("# Players", size=(10, 1)),
            sg.Spin([2, 3, 4], key="_num_players", size=(10, 1)),
        ],
        [
            sg.Text("Your name", size=(10, 1)),
            sg.InputText("", key="_player_name", size=(12, 1)),
        ],
    ]

    window_layout = [
        [sg.Frame("Setup", mode_layout)],
        [sg.Frame("Details", create_game_layout)],
        [sg.Button("Play", bind_return_key=True)],
    ]

    return window_layout
