#!/usr/bin/env python3

import pytest

from letters import Letter
from board import Board, Placement

b = None


def test_get_set_item():
    assert b[(0, 0)] is None, "Should be none"
    assert b[(1, 1)] == ".", "Location 1, 1 should be dot(.)"

    b[(1, 1)] = Letter("a", 1)
    assert isinstance(b[(1, 1)], Letter), "Location 1,1 should be letter object"


def test_place_word():
    letters = "klznie"
    b.place_word(letters, (1, 1), "h")
    assert isinstance(b[(1, 1)], Letter) and b[(1, 1)] == Letter.from_char(
        "k"
    ), "Location 1, 1 should be K"
    assert isinstance(b[(6, 1)], Letter) and b[(6, 1)] == Letter.from_char(
        "e"
    ), f"Location 6, 1 should be E, found {b[(6,1)]} instead"

    letters = "kart"
    b.place_word(letters, (1, 1), "v")
    assert isinstance(b[(1, 1)], Letter) and b[(1, 1)] == Letter.from_char(
        "k"
    ), "Location 1, 1 should be K"
    assert isinstance(b[(1, 4)], Letter) and b[(1, 4)] == Letter.from_char(
        "t"
    ), f"Location 1, 4 should be T, found {b[(1,4)]} instead"


def test_calculate_score():
    letters = "cat"
    # how much score would we get if we place this on 1,1
    score = b.calculate_score([Placement(letters, (1, 1), "h")])
    assert score == 15, "Score of placing 'cat' with TW shouldbe 15"


def test_should_not_calculate_bonus_twice():
    # now let's actually place it on 1,1
    b.place_word("cat", (1, 1))

    score = b.calculate_score([Placement("ar", (1, 2), "v")])
    assert score == 5, (
        "Extend word with bonus tile should not calculate the bonus twice. "
        f"Got {score}, should be 5"
    )

    b.place_word("ar", (1, 2), "v")
    # extend both of the 2 words using "t" making "at" from 2,1 and 1,2
    score = b.calculate_score([Placement("t", (2, 2), "h")])
    assert (
        score == 8
    ), f"Making 2 words from 2 neighboring words. Got {score}, should be 8"


def test_extending_word():
    b.place_word("test", (7, 8))
    score = b.calculate_score([Placement("star", (11, 8), "v")])
    assert score == 13, (
        "Another extending word test. Should get the score "
        f"from word tests and star. Got {score} but should be 13"
    )


def test_calculate_extending_2_words():
    b.place_word("cat", (7, 7), "v")
    b.place_word("taco", (9, 7), "v")
    score = b.calculate_score([Placement("a", (8, 7), "v")])
    assert score == 5, f"Extending 2 words test, should get 5 but got {score}"


def test_calculate_multiple_placements():
    b.place_word("cat", (7, 7), "v")
    b.place_word("taco", (9, 7), "v")
    score = b.calculate_score(
        [Placement("a", (8, 7), "h"), Placement("hode", (10, 7), "h")]
    )
    assert score == 14, (
        f"Extending 2 words test, should get 14 but got {score}. "
        "Word is cathode with last e on double letter"
    )


def test_multiple_placements():
    b.place_words([Placement("a", (1, 1), "h"), Placement("b", (3, 1), "h")])
    assert (
        b[(1, 1)].letter == "a" and b[(3, 1)].letter == "b"
    ), "2 letter should be placed separately."


def test_multiple_unplacements():
    b.place_words([Placement("a", (1, 1), "h"), Placement("b", (3, 1), "h")])
    b.unplace_words([Placement("a", (1, 1), "h"), Placement("b", (3, 1), "h")])
    assert (
        b[(1, 1)] == "." and b[(3, 1)] == "."
    ), "2 letter should be placed separately."


@pytest.fixture(scope="function", autouse=True)
def setup():
    global b
    b = Board()


if __name__ == "__main__":
    test_get_set_item()
    test_place_word()
    test_calculate_score()
    test_should_not_calculate_bonus_twice()
    test_extending_word()
    test_calculate_extending_2_words()
