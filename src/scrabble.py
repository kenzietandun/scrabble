#!/usr/bin/env python3

import board
import letters
import letters_bag
from typing import List


class Scrabble:
    """
    Defines a server overseeing a Scrabble game
    """

    def __init__(self, players):
        self.players = players
        self.curr_player = 0
        self.scores = [0 for _ in range(self.players)]
        self.player_names = [None for _ in range(self.players)]
        self.player_hands: List[List[str]] = [[] for _ in range(self.players)]
        self.board = board.Board()
        self.letters_bag = letters_bag.LettersBag()
        self.game_is_ending = False
        self.game_is_over = False
        self.last_play_player = None

    def get_board_state(self):
        return self.board.get_state()

    def join_game(self, player_name):
        if player_name in self.player_names:
            return self.player_names.index(player_name)

        try:
            print(self.player_names)
            empty_slot = self.player_names.index(None)
            self.player_names[empty_slot] = player_name
        except ValueError:
            return -1
        return empty_slot

    def get_board_json(self):
        state = self.board.get_state()
        letters = []
        for y, row in enumerate(state, start=1):
            for x, val in enumerate(row, start=1):
                if val == ".":
                    continue

                letters.append({"x": x, "y": y, "letter": val.letter})
        return letters

    def get_current_turn(self):
        return self.curr_player

    def get_board_bonuses(self):
        return self.board.BONUS

    def get_random_letters(self, amount, player_num):
        letters = self.letters_bag.get_random_letters(amount)
        print(f"Got random letters for player {player_num}: {letters}")
        self.player_hands[player_num].extend(letters)
        if not letters:
            self.game_is_ending = True

    def get_player_hand(self, player_num):
        return self.player_hands[player_num]

    def replace_letters(self, ls):
        print(f"Removing {ls} from player's hands")
        for l in ls:
            self.player_hands[self.curr_player].remove(l)

        ls = [letters.Letter.from_char(l) for l in ls]
        self.letters_bag.put_letters_back(ls)

        self.player_hands[self.curr_player].extend(
            self.letters_bag.get_random_letters(len(ls))
        )
        print(f"Player hand is now: {self.player_hands[self.curr_player]}")

    def predict_score(self, letters_coords):
        placements = board.generate_placements(letters_coords)
        num_letters = sum(map(lambda x: len(x.word), placements))
        score = self.board.calculate_score(placements, only_predict=True)
        if num_letters == 7:
            score += 50

        return score

    def place_words(self, letters_coords, blanks=[], all_used=False):
        print(f"Placing word.")
        print(f"Current player hand: {self.player_hands[self.curr_player]}")
        letters_used = [x[0] for x in letters_coords]
        print(f"Placing: {letters_used}")
        placements = board.generate_placements(letters_coords)
        self.board.validate_placement(placements)
        num_letters = sum(map(lambda x: len(x.word), placements))
        score = self.board.calculate_score(placements)
        if num_letters == 7:
            score += 50

        self.board.place_words(placements, blanks=blanks)
        for l in letters_used:
            self.player_hands[self.curr_player].remove(l)

        self.scores[self.curr_player] += score

        if self.game_is_ending and all_used:
            self.last_play_player = self.curr_player
            self.game_is_over = True


    def increment_turn(self):
        self.curr_player = (self.curr_player + 1) % self.players

    def is_game_over(self):
        return self.is_game_over

    def get_scores(self):
        return self.scores
