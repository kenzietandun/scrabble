#!/usr/bin/env python3

import random
import sys
import string

import board_bonus
import client
import PySimpleGUI as sg
import views

KEYS = ["t1", "t2", "t3", "t4", "t5", "t6", "t7"]


def set_all_tiles(window, disabled=True):
    for key in KEYS:
        window.element(key).update(disabled=disabled)
        if not window.element(key).get_text():
            window.element(key).update(disabled=True)


def enable_tile(letter, window):
    for key in KEYS:
        if letter == window.element(key).get_text() and window.element(key).is_disabled():
            window.element(key).update(disabled=False)
            break


def disable_tile(letter, window):
    for key in KEYS:
        if letter == window.element(key).get_text() and not window.element(key).is_disabled():
            window.element(key).update(disabled=True)
            break


def is_tile_available(letter, window):
    for key in KEYS:
        if (
            letter.upper() == window.element(key).get_text()
            and not window.element(key).is_disabled()
        ):
            return True
    print(f"Cannot find letter {letter}")
    return False


def get_available_tiles(window):
    available = []
    for key in KEYS:
        if not window.element(key).is_disabled():
            available.append(window.element(key).get_text())
    return available


def replace_tiles(player_hand, window):
    for key in KEYS:
        if player_hand:
            window.element(key).update(player_hand.pop(), disabled=False)
        else:
            window.element(key).update("", disabled=True)


def get_disabled_tiles(window):
    disabled = []
    for key in KEYS:
        if window.element(key).is_disabled():
            disabled.append(window.element(key).get_text())
    return disabled


def get_num_disabled_tiles(window):
    return len(get_disabled_tiles(window))


def remove_placed_tiles(state, window):
    for s in state:
        x = s["x"]
        y = s["y"]
        on_tile = window.element((x, y)).get()
        enabled = not window.element((x, y)).is_disabled()
        if on_tile and enabled:
            window.element((x, y)).update(value="")
            enable_tile(on_tile, window)


def update_board_state(state, window):
    for s in state:
        letter = s["letter"]
        x = s["x"]
        y = s["y"]
        window.element((x, y)).update(value=letter, disabled=True)


def toggle_tile_status(tile, window):
    status = window.element(tile).is_disabled()
    window.element(tile).update(disabled=not status)


def clear_board(window):
    for y in range(1, 16):
        for x in range(1, 16):
            if not window.element((x, y)).is_disabled():
                window.element((x, y)).update(value="")
    set_all_tiles(window, disabled=False)


def is_board_clear(window):
    for y in range(1, 16):
        for x in range(1, 16):
            if not window.element((x, y)).is_disabled() and window.element((x, y)).get():
                return False
    return True


def shuffle_tiles(window):
    tiles = []
    for key in KEYS:
        if window.element(key).get_text():
            tiles.append((window.element(key).get_text(), window.element(key).is_disabled()))

    random.shuffle(tiles)
    for i, key in enumerate(KEYS):
        tile_letter, tile_disabled = tiles[i]
        window.element(key).update(tile_letter, disabled=tile_disabled)


class ScrabbleGUI:
    def __init__(self):
        self.window = None
        self.event = None
        self.values = None
        self.prev_state = {}
        self.client = None

    def get_new_tiles(self):
        num_played_tiles = get_num_disabled_tiles(self.window)
        err = self.client.get_random_tiles(num_played_tiles)
        player_hand, err = self.client.get_player_hand()
        replace_tiles(player_hand, self.window)

    def not_changing_board_layout(self, coord, letter):
        return coord in self.prev_state and self.prev_state[coord] == letter

    def handle_input(self):
        """
        Handles user input on the board

        If the user erases an existing tile on the board
        remove from the played_tile list

        If the user adds a new tile to the board
        add to the played_tile list

        If the user adds a tile to the existing one on the
        board, keep the old tile and ignore the new tile
        """
        coord = self.event
        if self.window.element(coord).is_disabled():
            return
        if not self.values[coord]:  # erased
            if coord not in self.prev_state or not self.prev_state[coord]:
                return
            letter = self.prev_state[coord].upper()
            print(f"enabling letter {letter}")
            enable_tile(letter, self.window)
        elif len(self.values[coord]) >= 2:
            self.window.element(coord).update(value=self.prev_state[coord])
            return
        elif not is_tile_available(self.values[coord][-1], self.window):
            letter = self.values[coord][-1].upper()
            if self.not_changing_board_layout(coord, letter):
                return
            self.window.element(coord).update(value="")
            return
        else:
            letter = self.values[coord].upper()
            self.window.element(coord).update(value=letter.upper())
            if self.not_changing_board_layout(coord, letter):
                return
            print(f"disabling letter {letter}")
            disable_tile(letter, self.window)

        self.prev_state[coord] = self.values[coord].upper()

    def update_player_scores(self):
        self.player_names, err = self.client.get_players()
        player_scores, err = self.client.get_scores()
        for i, name in enumerate(self.player_names):
            self.window.element(f"_player{i}_name").update(value=name)
            self.window.element(f"_player{i}_score").update(value=player_scores[i])

    def update_board(self):
        new_state, err = self.client.get_board_state()
        remove_placed_tiles(new_state, self.window)
        update_board_state(new_state, self.window)
        self.update_player_scores()

    def run(self):
        self.setup_window()
        print(f"Playing as player {self.client.player_num}")
        self.main_window()

    def setup_window(self):
        window = sg.Window(
            "Scrabble", layout=views.get_setup_window_layout(), font="Helvetica 14"
        )
        while True:
            event, values = window.read()
            if not event:
                window.close()
                sys.exit()

            if event == "Play":
                room = values["_room_name"]
                num_players = int(values["_num_players"])
                name = values["_player_name"]
                create = values["_create"]
                break

        window.close()

        print(f"Setting up client")
        self.client = client.Client(
            room=room, player_name=name, total_players=num_players, room_master=create,
        )
        print(f"Finished setting up client: \n{self.client}")
        self.client.setup_game()

    def loading_window(self):
        pass

    def ask_blank_letter(self, num_blanks):
        if num_blanks == 1:
            layout = [
                [sg.Text("The blank represents:")],
                [sg.Combo(list(string.ascii_uppercase), key="_blank_1", size=(3, 1))],
                [sg.Button("Confirm")],
            ]
        else:
            layout = [
                [sg.Text("The blank represents:")],
                [
                    sg.Text("First blank"),
                    sg.Combo(list(string.ascii_uppercase), key="_blank_1", size=(3, 1)),
                ],
                [
                    sg.Text("Second blank"),
                    sg.Combo(list(string.ascii_uppercase), key="_blank_2", size=(3, 1)),
                ],
                [sg.Button("Confirm")],
            ]

        window = sg.Window("Blank Tile", layout=layout)
        _, values = window.read()
        blanks = [values["_blank_1"]]
        if num_blanks > 1:
            blanks.append(values["_blank_2"])
        window.close()

        return blanks

    def show_final_scores(self, unplayed_tiles):
        pass

    def main_window(self):
        self.prev_state = {}
        board_bonuses = board_bonus.BONUS
        total_tiles = 7
        err = self.client.get_random_tiles(total_tiles)
        player_hand, err = self.client.get_player_hand()

        self.window = sg.Window(
            "Scrabble",
            layout=views.get_main_window_layout(player_hand, board_bonuses),
            font=("Helvetica 14"),
            return_keyboard_events=True,
        ).finalize()
        self.update_player_scores()

        refresh = True
        is_our_turn = False
        checked = False

        curr_focus = (None, None)

        while True:
            self.event, self.values = self.window.read(
                timeout=5000
            )

            if refresh:
                # if self.client.is_game_over():
                #    unplayed_tiles = get_available_tiles(self.window)
                #    self.show_final_scores(unplayed_tiles)
                if not is_our_turn:
                    self.update_board()
                    checked = False
                elif not checked:
                    self.update_board()
                    checked = True

                is_our_turn = self.client.is_our_turn()
                self.window.element("_turn_info").update(self.player_names[self.client.turn])

            refresh = True

            if not self.event:
                break

            if self.event in KEYS:
                toggle_tile_status(self.event, self.window)

            elif ":" in self.event:  # keyboard events
                refresh = False
                if self.event in ["Up:111", "Down:116", "Left:113", "Right:114"]:
                    if not curr_focus[0]:
                        continue

                    if not isinstance(self.window.element(curr_focus), sg.InputText):
                        continue

                    print(f"Pressed button: {self.event}")
                    if self.event == "Up:111":
                        if curr_focus[1] == 1:
                            continue
                        if self.window.element((curr_focus[0], curr_focus[1]-1)).is_disabled():
                            continue
                        curr_focus = curr_focus[0], curr_focus[1] - 1
                    elif self.event == "Down:116":
                        if curr_focus[1] == 15:
                            continue
                        if self.window.element((curr_focus[0], curr_focus[1]+1)).is_disabled():
                            continue
                        curr_focus = curr_focus[0], curr_focus[1] + 1
                    elif self.event == "Left:113":
                        if curr_focus[0] == 1:
                            continue
                        if self.window.element((curr_focus[0]-1, curr_focus[1])).is_disabled():
                            continue
                        curr_focus = curr_focus[0] - 1, curr_focus[1]
                    elif self.event == "Right:114":
                        if curr_focus[0] == 15:
                            continue
                        if self.window.element((curr_focus[0]+1, curr_focus[1])).is_disabled():
                            continue
                        curr_focus = curr_focus[0] + 1, curr_focus[1]

                    print(f"Setting focus to element {curr_focus}")
                    self.window.element(curr_focus).set_focus()

            elif self.event == "Undo":
                refresh = False
                clear_board(self.window)

            elif self.event == "Shuffle":
                refresh = False
                shuffle_tiles(self.window)

            elif self.event == "Predict score":
                refresh = False
                newly_placed = []
                for coord in self.prev_state:
                    if self.prev_state[coord] and not self.window.element(coord).is_disabled():
                        letter = self.window.element(coord).get()
                        newly_placed.append((letter, coord))

                if not newly_placed:
                    continue

                score, err = self.client.predict_score(newly_placed)
                if err:
                    sg.PopupError(err)
                    continue

                self.window.element("_score_text").update(score)

            elif self.event == "Exchange":
                if not is_our_turn:
                    continue

                if not is_board_clear(self.window):
                    sg.PopupError("Please clear the board first")
                    continue

                tiles_to_exchange = get_disabled_tiles(self.window)
                if len(tiles_to_exchange) == 0:
                    continue

                err = self.client.exchange_letters(tiles_to_exchange)
                player_hand, err = self.client.get_player_hand()
                replace_tiles(player_hand, self.window)
                self.client.increment_turn()

            elif self.event == "Place":
                if not is_our_turn:
                    continue
                newly_placed = []
                blanks = []
                num_blanks = 0
                for coord in self.prev_state:
                    if self.prev_state[coord] and not self.window.element(coord).is_disabled():
                        letter = self.window.element(coord).get()
                        newly_placed.append((letter, coord))
                        if letter == "_":
                            num_blanks += 1

                if num_blanks > 0:
                    blanks = self.ask_blank_letter(num_blanks)

                if not newly_placed:
                    continue

                err = self.client.place_word(
                    newly_placed, blanks=blanks, all_used=get_num_disabled_tiles(self.window),
                )
                if err:
                    sg.PopupOK("Word placement is not valid")
                    continue

                new_state, err = self.client.get_board_state()
                update_board_state(new_state, self.window)

                self.get_new_tiles()
                set_all_tiles(self.window, disabled=False)
                self.update_player_scores()
                self.client.increment_turn()

            elif isinstance(self.event, tuple):  # Input on the board
                refresh = False
                curr_focus = self.event
                self.handle_input()


        self.window.close()


if __name__ == "__main__":
    sg.change_look_and_feel("DarkTeal9")
    ScrabbleGUI().run()
