#!/usr/bin/env python3

from flask import Flask, request, jsonify
import json
import sys
from typing import Dict
from scrabble import Scrabble
import errors

PORT = 1240
ADDR = "0.0.0.0"
ONGOING_GAME = False

app = Flask(__name__)
app.secret_key = "3kg02ltfL3prj93"

rooms: Dict[str, Scrabble] = {}


@app.route("/api/new/<room_id>", methods=["POST"])
def new_game(room_id):
    data = request.json
    num_players = data["num_players"]
    print(f"Creating room {room_id} for {num_players} players")
    rooms[room_id] = Scrabble(num_players)
    return jsonify({"status": "OK", "msg": f"Room {room_id} created", "player_num": 0})


@app.route("/api/join/<room_id>", methods=["POST"])
def join_game(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    data = request.json
    player_name = data["name"]

    scr_obj = rooms[room_id]
    player_num = scr_obj.join_game(player_name)
    if player_num == -1:
        return jsonify({"status": "KO", "msg": "Game room is full"})
    return jsonify({"status": "OK", "player_num": player_num})


@app.route("/api/get_players/<room_id>")
def get_players(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    return jsonify({"status": "OK", "names": scr_obj.player_names})


@app.route("/api/predict-score/<room_id>", methods=["POST"])
def predict_score(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    data = request.json
    scr_obj = rooms[room_id]
    letters_coords_json = data["letters_coords"]
    letters_coords = []
    for item in letters_coords_json:
        letter = item["letter"]
        coords = item["x"], item["y"]
        letters_coords.append((letter, coords))
    try:
        score = scr_obj.predict_score(letters_coords)
    except errors.NotValidPlacementError:
        return jsonify({"status": "KO", "msg": "Placement is invalid"})
    return jsonify({"status": "OK", "score": score})


if __name__ == "__main__":
    app.run(host=ADDR, port=PORT)


@app.route("/api/place/<room_id>", methods=["POST"])
def place(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    data = request.json
    scr_obj = rooms[room_id]
    letters_coords_json = data["letters_coords"]
    blanks = data["blanks"]
    all_used = data["all_used"]
    letters_coords = []
    for item in letters_coords_json:
        letter = item["letter"]
        coords = item["x"], item["y"]
        letters_coords.append((letter, coords))
    try:
        scr_obj.place_words(letters_coords, blanks=blanks, all_used=all_used)
    except errors.NotValidPlacementError:
        return jsonify({"status": "KO", "msg": "Placement is invalid"})
    return jsonify({"status": "OK"})


@app.route("/api/tiles/<room_id>", methods=["POST"])
def get_tiles(room_id):
    data = request.json
    num_tiles = data["num_tiles"]
    player_num = data["player_num"]
    scr_obj = rooms[room_id]
    try:
        scr_obj.get_random_letters(int(num_tiles), player_num)
    except ValueError:
        return jsonify({"status": "KO", "msg": "Invalid value of tiles"})
    return {"status": "OK"}


@app.route("/api/turn/<room_id>")
def get_current_turn(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    return jsonify({"status": "OK", "player_turn": scr_obj.get_current_turn()})


@app.route("/api/state/<room_id>")
def get_state(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    return jsonify({"status": "OK", "board": scr_obj.get_board_json()})


@app.route("/api/score/<room_id>")
def get_score(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    return jsonify({"status": "OK", "scores": scr_obj.get_scores()})


@app.route("/api/exchange/<room_id>", methods=["POST"])
def exchange(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    data = request.json
    scr_obj = rooms[room_id]
    scr_obj.replace_letters(data["tiles"])
    return jsonify({"status": "OK"})


@app.route("/api/player-hand/<room_id>/<num>")
def get_player_hand(room_id, num):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    return jsonify({"status": "OK", "player_hand": scr_obj.get_player_hand(int(num))})


@app.route("/api/end-turn/<room_id>", methods=["POST"])
def end_turn(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    scr_obj.increment_turn()
    return jsonify({"status": "OK"})


@app.route("/api/game-over/<room_id>")
def is_game_over(room_id):
    if not room_id in rooms:
        return jsonify({"status": "KO", "msg": "Room ID is invalid"})

    scr_obj = rooms[room_id]
    return jsonify({"status": "OK", "game_over": scr_obj.is_game_over()})
