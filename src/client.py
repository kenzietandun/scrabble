#!/usr/bin/env python3

import requests

#SERVER = "http://149.28.169.63:1240"
SERVER = "http://localhost:1240"


class Client:
    def __init__(self, room="", player_name="", total_players=-1, room_master=False):
        self.room = room
        self.player_name = player_name
        self.total_players = total_players
        self.room_master = room_master
        self.turn = -1

        self.player_num = -1

    def __str__(self):
        return f"Room: {self.room}, Player Name: {self.player_name}, Room Master: {self.room_master}"

    def setup_game(self):
        if self.room_master:
            print(f"Creating room {self.room}")
            self.create_room()
        print(f"Joining room {self.room}")
        self.join_room()

    def create_room(self):
        payload = {"num_players": self.total_players}
        resp = requests.post(f"{SERVER}/api/new/{self.room}", json=payload)
        return resp.json()

    def join_room(self):
        payload = {"name": self.player_name}
        resp = requests.post(f"{SERVER}/api/join/{self.room}", json=payload).json()
        if resp["status"] == "KO":
            return None, resp["msg"]
        self.player_num = resp["player_num"]
        return resp["player_num"], ""

    def get_scores(self):
        resp = requests.get(f"{SERVER}/api/score/{self.room}").json()
        return resp["scores"], ""

    def get_players(self):
        resp = requests.get(f"{SERVER}/api/get_players/{self.room}").json()
        return resp["names"], ""

    def is_our_turn(self):
        self.turn, _ = self.get_turn()
        return self.turn == self.player_num

    def predict_score(self, letters_coords):
        items = []
        for item in letters_coords:
            item_json = {"letter": item[0], "x": item[1][0], "y": item[1][1]}
            items.append(item_json)

        payload = {"letters_coords": items}
        resp = requests.post(f"{SERVER}/api/predict-score/{self.room}", json=payload).json()

        if resp["status"] == "KO":
            return None, resp["msg"]
        return resp["score"], ""

    def place_word(self, letters_coords, blanks=[], all_used=False):
        items = []
        for item in letters_coords:
            item_json = {"letter": item[0], "x": item[1][0], "y": item[1][1]}
            items.append(item_json)

        payload = {"letters_coords": items, "blanks": blanks, "all_used": all_used}
        resp = requests.post(f"{SERVER}/api/place/{self.room}", json=payload).json()

        if resp["status"] == "KO":
            return resp["msg"]
        return ""

    def get_random_tiles(self, num):
        payload = {"num_tiles": num, "player_num": self.player_num}
        resp = requests.post(f"{SERVER}/api/tiles/{self.room}", json=payload).json()
        if resp["status"] == "OK":
            return ""

        return resp["msg"]

    def get_turn(self):
        resp = requests.get(f"{SERVER}/api/turn/{self.room}").json()
        if resp["status"] == "OK":
            return resp["player_turn"], ""
        return None, resp["msg"]

    def get_board_state(self):
        resp = requests.get(f"{SERVER}/api/state/{self.room}").json()
        if resp["status"] == "OK":
            return resp["board"], ""
        return None, resp["msg"]

    def exchange_letters(self, letters):
        payload = {"tiles": letters}
        resp = requests.post(f"{SERVER}/api/exchange/{self.room}", json=payload).json()
        if resp["status"] == "OK":
            return ""
        return resp["msg"]

    def get_player_hand(self):
        resp = requests.get(f"{SERVER}/api/player-hand/{self.room}/{self.player_num}").json()
        if resp["status"] == "OK":
            return resp["player_hand"], ""
        return None, resp["msg"]

    def increment_turn(self):
        resp = requests.post(f"{SERVER}/api/end-turn/{self.room}").json()
        if resp["status"] == "OK":
            return ""
        return resp["msg"]

    def is_game_over(self):
        resp = requests.get(f"{SERVER}/api/game-over/{self.room}").json()
        if resp["status"] == "OK":
            return resp["game_over"], ""
        return None, resp["msg"]
