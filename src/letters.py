#!/usr/bin/env python3

"""
Defines class and constants
for Letters in a scrabble game
"""

from typing import List

LETTERS_SCORE = {
    "_": 0,
    "A": 1,
    "B": 3,
    "C": 3,
    "D": 2,
    "E": 1,
    "F": 4,
    "G": 2,
    "H": 4,
    "I": 1,
    "J": 8,
    "K": 5,
    "L": 1,
    "M": 3,
    "N": 1,
    "O": 1,
    "P": 3,
    "Q": 10,
    "R": 1,
    "S": 1,
    "T": 1,
    "U": 1,
    "V": 4,
    "W": 4,
    "X": 8,
    "Y": 4,
    "Z": 10,
}


class Letter:
    """
    Defines a Letter object.
    Letter obj has a letter it's representing and its
    score in scrabble.
    """

    def __init__(self, letter: str, point: int):
        self.letter = letter
        self.point = point

    def __hash__(self):
        return hash((self.letter, self.point))

    def __eq__(self, other: object):
        if not isinstance(other, Letter):
            return NotImplemented

        return (self.letter, self.point) == (other.letter, other.point)

    def __str__(self):
        return f"{self.letter}"

    def __repr__(self):
        return f"{self.letter}"

    @classmethod
    def from_str(cls, string: str) -> List["Letter"]:
        """
        Generates a list of Letter objs from a
        string representation
        """
        letters = []
        for letter in string:
            letters.append(Letter(letter, LETTERS_SCORE[letter]))

        return letters

    @classmethod
    def from_char(cls, char: str) -> "Letter":
        """
        Generates a Letter obj from a
        single char representation
        """
        return Letter(char.upper(), LETTERS_SCORE[char])
