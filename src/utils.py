#!/usr/bin/env python3

import socket
import pickle


def send_data(host, port, data):
    data = {"letter": "a", "location": (4, 4), "orientation": "h"}
    msg = pickle.dumps(data)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((host, port))
        s.sendall(msg)
        recvd = s.recv(1024)

    recvd = pickle.loads(recvd)
    return recvd
