#!/usr/bin/env python3

import os
from collections import namedtuple
from typing import List, Tuple, Union

import errors
import board_bonus
from letters import Letter


def above(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate above of current location
    """
    return curr_location[0], curr_location[1] - 1


def below(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate below of current location
    """
    return curr_location[0], curr_location[1] + 1


def left(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate to the left of current location
    """
    return curr_location[0] - 1, curr_location[1]


def right(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate to the right of current location
    """
    return curr_location[0] + 1, curr_location[1]


# Defines a placement of a word on the board
Placement = namedtuple("Placement", ["word", "location", "orientation"])


def get_placement_surroundings(p: Placement):
    length = len(p.word)
    orient = p.orientation
    x, y = p.location

    surroundings = []

    if orient == "h":
        tail_x = x + length - 1
        if x >= 2:
            surroundings.append((x - 1, y))
        if tail_x <= Board.LENGTH - 1:
            surroundings.append((tail_x + 1, y))
    else:
        tail_y = y + length - 1
        if y >= 2:
            surroundings.append((x, y - 1))
        if tail_y <= Board.HEIGHT - 1:
            surroundings.append((x, tail_y + 1))

    for i in range(length):
        if orient == "v":
            if x >= 2:
                surroundings.append((x - 1, y + i))
            if x <= Board.LENGTH - 1:
                surroundings.append((x + 1, y + i))
        elif orient == "h":
            if y >= 2:
                surroundings.append((x + i, y - 1))
            if y <= Board.HEIGHT - 1:
                surroundings.append((x + i, y + 1))

    return surroundings


def generate_placements(letters_coords: List[Tuple[str, Tuple[int, int]]]):
    print(f"Generating placement for {letters_coords}")

    coords = list(map(lambda x: x[1], letters_coords))
    is_vertical = all(x == coords[0][0] for x, _ in coords)
    is_horizontal = all(y == coords[0][1] for _, y in coords)
    letters_coords.sort(key=lambda x: x[1][1] if is_vertical else x[1][0])

    print(f"Sorted placement to {letters_coords}")
    print(f"Word is vertical: {is_vertical}")
    print(f"Word is horizontal: {is_horizontal}")

    if not (is_vertical or is_horizontal):
        raise errors.NotValidPlacementError

    def is_adjacent(coord1, coord2):
        if is_vertical:
            return abs(coord1[1] - coord2[1]) == 1

        # horizontal
        return abs(coord1[0] - coord2[0]) == 1

    def split_to_adjacent_groups(letters_coords):
        groups = []
        adj_group = []
        for letter, coords in letters_coords:
            if not adj_group:
                adj_group.append((letter, coords))

            elif is_adjacent(adj_group[-1][1], coords):
                adj_group.append((letter, coords))
            else:
                groups.append(adj_group)
                adj_group = [(letter, coords)]

        if adj_group:
            groups.append(adj_group)
        return groups

    def adj_group_to_placement(adj_group):
        first_elem = adj_group[0]
        start_coord = first_elem[1]
        word = "".join([x[0] for x in adj_group])
        orient = "h" if is_horizontal else "v"
        return Placement(word, start_coord, orient)

    adj_groups = split_to_adjacent_groups(letters_coords)
    print(f"Generated adj groups {adj_groups}")
    placements = [adj_group_to_placement(a) for a in adj_groups]

    print(f"Generated placement {placements}")

    return placements


class Board:
    """
    Defines a board and the actions on a board
    """

    DOUBLE_LETTER = board_bonus.DOUBLE_LETTER
    DOUBLE_WORD = board_bonus.DOUBLE_WORD
    TRIPLE_LETTER = board_bonus.TRIPLE_LETTER
    TRIPLE_WORD = board_bonus.TRIPLE_WORD
    BONUS = board_bonus.BONUS

    LENGTH = HEIGHT = 15

    def __init__(self):
        """
        Initialises clean board for a game of scrabble
        """
        self.board_x = Board.LENGTH
        self.board_y = Board.HEIGHT
        self.spacing = 4
        self.board: List[List[Letter]] = [
            ["." for j in range(self.board_x)] for i in range(self.board_y)
        ]
        self.clean_state = True

    def __getitem__(self, location: Tuple[int, int]) -> Letter:
        """
        Returns the item on location
        """
        x, y = location
        if x < 1 or x > Board.LENGTH or y < 1 or y > Board.HEIGHT:
            return None
        return self.board[y - 1][x - 1]

    def __setitem__(self, location: Tuple[int, int], value: Letter):
        """
        Sets item on the board with a Letter obj
        """
        x, y = location
        if x < 1 or x > Board.LENGTH or y < 1 or y > Board.HEIGHT:
            raise IndexError("Error placing letter out of board")
        self.board[y - 1][x - 1] = value

    def print_board(self):
        """
        Prints the current state of the board
        """
        os.system("clear")
        self._print_board_x_axis_indicator()
        print()
        for i, row in enumerate(self.board, start=1):
            self._print_board_y_axis_indicator(i)
            for col in row:
                print(f"{col}", end=" " * 4)
            print("\n")

    def _print_board_x_axis_indicator(self):
        """
        Prints the numbers above the board
        indicating the x axis
        """
        for i in range(Board.LENGTH + 1):
            print(f"{i:{self.spacing}}", end=" ")
        print()

    def _print_board_y_axis_indicator(self, number: int):
        """
        Prints the numbers on the left hand side of the board
        indicating the y axis
        """
        print(
            f"{number:{self.spacing}}", end=" " * (self.spacing),
        )

    def get_state(self) -> List[List[Union[str, Letter]]]:
        """
        Returns the current state of the board
        """
        return self.board

    def place_word(
        self,
        word: str,
        location: Tuple[int, int],
        orientation: str = "h",
        only_predict: bool = False,
        blanks: List[str] = [],
    ) -> List[str]:
        """
        Places word on the board specified by location tuple (x, y).
        Orientation specifies whether the word is going to be placed
        horizontally or vertically.
        """
        if not word:
            raise Exception("Word cannot be empty!")

        print(f"Attempting to place {word} in {location} with orientation {orientation}")

        x, y = location

        word_length = len(word)
        print(f"Word length is {word_length}")

        if orientation == "h" and (word_length - 1 + x > self.board_x or x < 0):
            raise Exception("Cannot place word over the maximum board length")

        if orientation == "v" and (word_length - 1 + y > self.board_y or y < 0):
            raise Exception("Cannot place word over the maximum board height")

        for i, letter in enumerate(word):
            letter_obj = Letter.from_char(letter)
            if letter == "_" and blanks:
                letter_obj = Letter.from_char(blanks.pop(0))
                letter_obj.point = 0

            if orientation == "h":
                print(f"Placing Letter {letter} on {x+i},{y}")
                self[(x + i, y)] = letter_obj
            else:
                print(f"Placing Letter {letter} on {x},{y+i}")
                self[(x, y + i)] = letter_obj

        if not only_predict:
            print("Changing clean_state to False")
            self.clean_state = False

    def _unplace_word(self, word: str, location: Tuple[int, int], orientation: str):
        """
        Unplaces a word from the board
        """
        curr_location = location
        for _ in word:
            self[curr_location] = "."

            if orientation == "h":
                curr_location = right(curr_location)
            else:
                curr_location = below(curr_location)

    def place_words(self, placements: List[Placement], blanks=[]):
        for placement in placements:
            word, location, orient = placement
            self.place_word(word, location, orient, blanks=blanks)

    def unplace_words(self, placements: List[Placement]):
        for placement in placements:
            word, location, orient = placement
            self._unplace_word(word, location, orient)

    def validate_placement(self, placements: List[Placement]):
        """
        Checks if the list of placements are extending
        off another tiles on the board
        """
        print("Validating placement")
        print(f"The board is in a clean state: {self.clean_state}")
        if self.clean_state:
            if len(placements) > 1:
                raise errors.NotValidPlacementError

            x, y = placements[0].location
            length = len(placements[0].word) - 1
            print(x, y, length)
            if not (
                (x <= 8 <= x + length and y == 8) or (y <= 8 <= y + length and x == 8)
            ):
                raise errors.NotValidPlacementError
            return

        for placement in placements:
            surroundings = get_placement_surroundings(placement)
            print(f"Surroundings: {surroundings}")
            print([isinstance(self[s], Letter) for s in surroundings])
            if not any([isinstance(self[s], Letter) for s in surroundings]):
                raise errors.NotValidPlacementError

    def calculate_score(self, placements: List[Placement], only_predict=True) -> int:
        """calculate_score

        calculates the score if a word is placed on the specified location.

        if the word made is valid, add the score of the word, taking account
        bonus (DoubleWord, TripleWord, DoubleLetter, TripleLetter) on that
        location as well.

        if a word is found to be extending another word, calculates the
        score of the extended word as well

        :param word: the word
        :param location: word placement
        :param only_predict: True to enable valid English word checking, False otherwise
        :returns: score
        """
        extended_words = []
        for placement in placements:
            word, location, orientation = placement
            self.place_word(word, location, orientation, only_predict=only_predict)

            extended_words.extend(
                self._get_generated_words(word, location, orientation)
            )

        score = 0
        extended_words.append(self._find_longest_word(location, orientation))

        for placement in placements:
            word, location, orientation = placement
            self._unplace_word(word, location, orientation)

        for e_word, start, direction in extended_words:
            if e_word:
                if self._is_valid(e_word) or only_predict:
                    score += self._get_score(e_word, start, direction)
                else:
                    raise errors.NotValidWordError

        return score

    def _get_generated_words(
        self, word: str, location: Tuple[int, int], orientation: str
    ):
        """
        Find the list of generated words if we put this word in the
        location specified

        :param word: the word
        :param location: starting location of the word
        :param orientation: orientation of the word
        :returns: List of generated words (tuple of the word,
                  starting location and orientation)
        """
        curr_location = location
        generated_words = []
        for _ in word:
            if orientation == "h":
                generated_words.append(self._find_longest_word(curr_location, "v"))
                curr_location = right(curr_location)
            else:
                generated_words.append(self._find_longest_word(curr_location, "h"))
                curr_location = below(curr_location)

        return generated_words

    def _find_longest_word(self, location: Tuple[int, int], orientation: str):
        """
        Given a location and orientation, find the longest
        word we can make. The word does not have to be valid.

        :param location: location of the starting letter
        :param orientation: orientation of the word
        :returns: Tuple of longest word, starting location and orientation
        """
        moving_direction = above if orientation == "v" else left
        word = self[location].letter

        start = location
        curr_location = moving_direction(location)
        while isinstance(self[curr_location], Letter):
            word = self[curr_location].letter + word
            start = curr_location
            curr_location = moving_direction(curr_location)

        moving_direction = below if orientation == "v" else right
        curr_location = moving_direction(location)
        while isinstance(self[curr_location], Letter):
            word += self[curr_location].letter
            curr_location = moving_direction(curr_location)

        if len(word) > 1:
            return word, start, orientation

        return "", start, orientation

    def _is_valid(self, word):
        return True

    def _get_score(self, word: str, location: Tuple[int, int], orientation: str):
        """get_score

        Given a word on a location on the board with a certain orientation
        (horizontal or vertical), calculate the score that playing the score
        yields us.

        the tricky part is when the word that we want to calculate the score of
        is extending another word and the letter which extends the word is sitting
        on a score multiplier, so this function must keep track of board bonuses
        that have been used before should not be used again.

        the way it does that is by checking if there is already a letter on the board
        if there's a letter that means the multiplier has been used before and will
        not contribute towards the score.

        :param word: the word we are placing
        :param location: location of the word
        :param orientation: h or v
        :returns: the score of placing that word
        """
        score = 0

        curr_location = location
        score_multiplier = 0
        for letter in Letter.from_str(word):
            letter_points = letter.point
            curr_location_occupied = isinstance(self[curr_location], Letter)
            loc_bonus = Board.BONUS.get(curr_location, "")
            if loc_bonus and not curr_location_occupied:
                if loc_bonus == Board.DOUBLE_LETTER:
                    letter_points *= 2
                elif loc_bonus == Board.TRIPLE_LETTER:
                    letter_points *= 3

                if loc_bonus == Board.DOUBLE_WORD:
                    score_multiplier += 2
                elif loc_bonus == Board.TRIPLE_WORD:
                    score_multiplier += 3
            score += letter_points

            if orientation == "v":
                curr_location = below(curr_location)
            elif orientation == "h":
                curr_location = right(curr_location)

        print(f"Score before multiplier: {score}")
        return score * (1 if not score_multiplier else score_multiplier)
