#!/usr/bin/env python3


class NotValidWordError(Exception):
    pass


class NotValidPlacementError(Exception):
    pass
