#!/usr/bin/env python3

"""
Defines Letter Bag object in a game of scrabble
"""

import random
from typing import Dict, List
from letters import Letter
from letters import LETTERS_SCORE


class LettersBag:
    """
    Defines a Letters bag object in a scrabble game

    A letter bag contains a predefined number Letters

    A Player can take a certain amount of letters from
    the bag each turn
    """

    def __init__(self):
        self.letters = {
            #Letter("_", LETTERS_SCORE["_"]): 0,
            #Letter("A", LETTERS_SCORE["A"]): 0,
            #Letter("B", LETTERS_SCORE["B"]): 0,
            #Letter("C", LETTERS_SCORE["C"]): 0,
            #Letter("D", LETTERS_SCORE["D"]): 0,
            #Letter("E", LETTERS_SCORE["E"]): 0,
            #Letter("F", LETTERS_SCORE["F"]): 0,
            #Letter("G", LETTERS_SCORE["G"]): 0,
            #Letter("H", LETTERS_SCORE["H"]): 0,
            #Letter("I", LETTERS_SCORE["I"]): 0,
            #Letter("J", LETTERS_SCORE["J"]): 0,
            #Letter("K", LETTERS_SCORE["K"]): 0,
            #Letter("L", LETTERS_SCORE["L"]): 0,
            #Letter("M", LETTERS_SCORE["M"]): 0,
            #Letter("N", LETTERS_SCORE["N"]): 0,
            #Letter("O", LETTERS_SCORE["O"]): 0,
            #Letter("P", LETTERS_SCORE["P"]): 0,
            #Letter("Q", LETTERS_SCORE["Q"]): 0,
            #Letter("R", LETTERS_SCORE["R"]): 0,
            #Letter("S", LETTERS_SCORE["S"]): 0,
            #Letter("T", LETTERS_SCORE["T"]): 0,
            #Letter("U", LETTERS_SCORE["U"]): 10,
            #Letter("V", LETTERS_SCORE["V"]): 10,
            #Letter("W", LETTERS_SCORE["W"]): 10,
            #Letter("X", LETTERS_SCORE["X"]): 2,
            #Letter("Y", LETTERS_SCORE["Y"]): 2,
            #Letter("Z", LETTERS_SCORE["Z"]): 3,
            Letter("_", LETTERS_SCORE["_"]): 2,
            Letter("A", LETTERS_SCORE["A"]): 9,
            Letter("B", LETTERS_SCORE["B"]): 2,
            Letter("C", LETTERS_SCORE["C"]): 2,
            Letter("D", LETTERS_SCORE["D"]): 4,
            Letter("E", LETTERS_SCORE["E"]): 12,
            Letter("F", LETTERS_SCORE["F"]): 2,
            Letter("G", LETTERS_SCORE["G"]): 3,
            Letter("H", LETTERS_SCORE["H"]): 2,
            Letter("I", LETTERS_SCORE["I"]): 9,
            Letter("J", LETTERS_SCORE["J"]): 1,
            Letter("K", LETTERS_SCORE["K"]): 1,
            Letter("L", LETTERS_SCORE["L"]): 4,
            Letter("M", LETTERS_SCORE["M"]): 2,
            Letter("N", LETTERS_SCORE["N"]): 6,
            Letter("O", LETTERS_SCORE["O"]): 8,
            Letter("P", LETTERS_SCORE["P"]): 2,
            Letter("Q", LETTERS_SCORE["Q"]): 1,
            Letter("R", LETTERS_SCORE["R"]): 6,
            Letter("S", LETTERS_SCORE["S"]): 4,
            Letter("T", LETTERS_SCORE["T"]): 6,
            Letter("U", LETTERS_SCORE["U"]): 4,
            Letter("V", LETTERS_SCORE["V"]): 2,
            Letter("W", LETTERS_SCORE["W"]): 2,
            Letter("X", LETTERS_SCORE["X"]): 1,
            Letter("Y", LETTERS_SCORE["Y"]): 2,
            Letter("Z", LETTERS_SCORE["Z"]): 1,
        }

    def get_random_letters(self, amount: int) -> List[str]:
        """
        Gets random letters from the bag and removing it permanently
        """
        curr_amount = 0
        random_letters = []

        while curr_amount < amount:
            avail_letters = [x[0] for x in self.letters.items() if x[1] > 0]
            if not avail_letters:
                return []

            random_letter = random.choice(avail_letters)
            print(f"Adding {random_letter} to list of obtained letters")
            self.letters[random_letter] -= 1
            curr_amount += 1
            random_letters.append(random_letter.letter)

        print(f"Got {random_letters} from the bag")
        return random_letters

    def put_letters_back(self, letters: List[Letter]):
        """
        Put some letters back into the bag
        """
        for letter in letters:
            self.letters[letter] += 1

    def is_empty(self):
        """
        Returns True if the letters bag has no more letters
        """
        return sum(self.letters.values()) == 0
