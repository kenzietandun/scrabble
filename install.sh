#!/bin/bash

# Install python modules
sudo apt install python3-pip python3-venv

# Clone project
git clone https://gitlab.com/kenzietandun/scrabble && \
  cd scrabble

# Install dependencies
python3 -m venv venv && \
  source venv/bin/activate && \
  pip3 install -r requirements.txt
