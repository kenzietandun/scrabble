#!/usr/bin/env python3

from typing import List, Tuple
import os
from letters import Letter
from collections import namedtuple


def above(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate above of current location
    """
    return curr_location[0], curr_location[1] - 1


def below(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate below of current location
    """
    return curr_location[0], curr_location[1] + 1


def left(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate to the left of current location
    """
    return curr_location[0] - 1, curr_location[1]


def right(curr_location: Tuple[int, int]) -> Tuple[int, int]:
    """
    Get a coordinate to the right of current location
    """
    return curr_location[0] + 1, curr_location[1]


Placement = namedtuple("Placement", ["word", "location", "orientation"])

class Board:
    """
    Defines a board and the actions on a board
    """

    DOUBLE_LETTER = "DL"
    DOUBLE_WORD = "DW"
    TRIPLE_LETTER = "TL"
    TRIPLE_WORD = "TW"

    BONUS = {
        (1, 1): TRIPLE_WORD,
        (8, 1): TRIPLE_WORD,
        (15, 1): TRIPLE_WORD,
        (1, 8): TRIPLE_WORD,
        (15, 8): TRIPLE_WORD,
        (1, 15): TRIPLE_WORD,
        (15, 15): TRIPLE_WORD,
        (2, 2): DOUBLE_WORD,
        (3, 3): DOUBLE_WORD,
        (4, 4): DOUBLE_WORD,
        (5, 5): DOUBLE_WORD,
        (8, 8): DOUBLE_WORD,
        (11, 11): DOUBLE_WORD,
        (12, 12): DOUBLE_WORD,
        (13, 13): DOUBLE_WORD,
        (14, 14): DOUBLE_WORD,
        (2, 14): DOUBLE_WORD,
        (3, 13): DOUBLE_WORD,
        (4, 12): DOUBLE_WORD,
        (5, 11): DOUBLE_WORD,
        (11, 5): DOUBLE_WORD,
        (12, 4): DOUBLE_WORD,
        (13, 3): DOUBLE_WORD,
        (14, 2): DOUBLE_WORD,
        (4, 1): DOUBLE_LETTER,
        (12, 1): DOUBLE_LETTER,
        (7, 3): DOUBLE_LETTER,
        (9, 3): DOUBLE_LETTER,
        (1, 4): DOUBLE_LETTER,
        (8, 4): DOUBLE_LETTER,
        (15, 4): DOUBLE_LETTER,
        (3, 7): DOUBLE_LETTER,
        (7, 7): DOUBLE_LETTER,
        (9, 7): DOUBLE_LETTER,
        (13, 7): DOUBLE_LETTER,
        (4, 8): DOUBLE_LETTER,
        (12, 8): DOUBLE_LETTER,
        (3, 9): DOUBLE_LETTER,
        (7, 9): DOUBLE_LETTER,
        (9, 9): DOUBLE_LETTER,
        (13, 9): DOUBLE_LETTER,
        (1, 12): DOUBLE_LETTER,
        (8, 12): DOUBLE_LETTER,
        (15, 12): DOUBLE_LETTER,
        (7, 13): DOUBLE_LETTER,
        (9, 13): DOUBLE_LETTER,
        (4, 15): DOUBLE_LETTER,
        (12, 15): DOUBLE_LETTER,
        (6, 2): TRIPLE_LETTER,
        (10, 2): TRIPLE_LETTER,
        (2, 6): TRIPLE_LETTER,
        (6, 6): TRIPLE_LETTER,
        (10, 6): TRIPLE_LETTER,
        (14, 6): TRIPLE_LETTER,
        (2, 10): TRIPLE_LETTER,
        (6, 10): TRIPLE_LETTER,
        (10, 10): TRIPLE_LETTER,
        (14, 10): TRIPLE_LETTER,
        (6, 14): TRIPLE_LETTER,
        (10, 14): TRIPLE_LETTER,
    }

    LENGTH = HEIGHT = 15

    def __init__(self):
        """
        Initialises clean board for a game of scrabble
        """
        self.board_x = Board.LENGTH
        self.board_y = Board.HEIGHT
        self.spacing = 4
        self.board: List[List[Letter]] = [
            ["." for j in range(self.board_x)] for i in range(self.board_y)
        ]

    def __getitem__(self, location: Tuple[int, int]) -> Letter:
        """
        Returns the item on location
        """
        x, y = location
        if x < 1 or x > Board.LENGTH or y < 1 or y > Board.HEIGHT:
            return None
        return self.board[y - 1][x - 1]

    def __setitem__(self, location: Tuple[int, int], value: Letter):
        """
        Sets item on the board with a Letter obj
        """
        x, y = location
        if x < 1 or x > Board.LENGTH or y < 1 or y > Board.HEIGHT:
            raise IndexError("Error placing letter out of board")
        self.board[y - 1][x - 1] = value

    def print_board(self):
        """
        Prints the current state of the board
        """
        os.system("clear")
        self._print_board_x_axis_indicator()
        print()
        for i, row in enumerate(self.board, start=1):
            self._print_board_y_axis_indicator(i)
            for col in row:
                print(f"{col}", end=" ")
            print("\n")

    def _print_board_x_axis_indicator(self):
        """
        Prints the numbers above the board
        indicating the x axis
        """
        for i in range(Board.LENGTH + 1):
            print(f"{i:{self.spacing}}", end=" ")
        print()

    def _print_board_y_axis_indicator(self, number: int):
        """
        Prints the numbers on the left hand side of the board
        indicating the y axis
        """
        print(
            f"{number:{self.spacing}}", end=" " * (self.spacing),
        )

    def place_word(
        self, word: str, location: Tuple[int, int], orientation: str = "h",
    ) -> List[str]:
        """
        Places word on the board specified by location tuple (x, y).
        Orientation specifies whether the word is going to be placed
        horizontally or vertically.
        """
        if not word:
            raise Exception("Word cannot be empty!")

        x, y = location

        word_length = len(word)
        if orientation == "h" and (word_length + x > self.board_x or x < 0):
            raise Exception("Cannot place word over the maximum board length")

        if orientation == "v" and (word_length + y > self.board_y or y < 0):
            raise Exception("Cannot place word over the maximum board height")

        if not self._is_word_placement_valid(word, location, orientation):
            raise Exception("Word placement does not match existing letters on board")

        used_letters = []

        for i, letter in enumerate(word):
            if orientation == "h":
                if not isinstance(self[(x + i, y)], Letter):
                    used_letters.append(letter)
                self[(x + i, y)] = Letter.from_char(letter)
            else:
                if not isinstance(self[(x, y + i)], Letter):
                    used_letters.append(letter)
                self[(x, y + i)] = Letter.from_char(letter)

        return used_letters

    def _unplace_word(self, word: str, location: Tuple[int, int], orientation: str):
        """
        Unplaces a word from the board
        """
        curr_location = location
        for _ in word:
            self[curr_location] = "."

            if orientation == "h":
                curr_location = right(curr_location)
            else:
                curr_location = below(curr_location)

    def place_words(self, placements: List[Placement]):
        for placement in placements:
            word, location, orient = placement
            self.place_word(word, location, orient)

    def unplace_words(self, placements: List[Placement]):
        for placement in placements:
            word, location, orient = placement
            self._unplace_word(word, location, orient)

    def _is_word_placement_valid(
        self, word: str, location: Tuple[int, int], orientation: str,
    ) -> bool:
        """
        checks if placing the word in the specified location and orientation
        is a valid move, the letters that are going to be placed must match
        with the existing letters on the board
        """
        x, y = location

        word = Letter.from_str(word)

        for i, letter in enumerate(word):
            if orientation == "h" and self[(x + i, y)] and self[(x + i, y)] != ".":
                if letter != self[(x + i, y)]:
                    return False
            elif orientation == "v" and self[(x, y + i)] and self[(x, y + i)] != ".":
                if letter != self[(x, y + i)]:
                    return False

        return True

    def calculate_score(self, placements: List[Placement]) -> int:
        """calculate_score

        calculates the score if a word is placed on the specified location.

        if the word made is valid, add the score of the word, taking account
        bonus (DoubleWord, TripleWord, DoubleLetter, TripleLetter) on that
        location as well.

        if a word is found to be extending another word, calculates the
        score of the extended word as well

        :param word: the word
        :param location: word placement
        """
        extended_words = []
        for placement in placements:
            word, location, orientation = placement
            self.place_word(word, location, orientation)

            extended_words.extend(self._get_generated_words(word, location, orientation))

        score = 0
        extended_words.append(self._find_longest_word(location, orientation))

        for placement in placements:
            word, location, orientation = placement
            self._unplace_word(word, location, orientation)

        for e_word, start, direction in extended_words:
            if e_word and self._is_valid(e_word):
                score += self._get_score(e_word, start, direction)

        return score

    def _get_generated_words(self, word: str, location: Tuple[int, int], orientation: str):
        """
        Find the list of generated words if we put this word in the
        location specified
   
        :param word: the word
        :param location: starting location of the word
        :param orientation: orientation of the word
        :returns: List of generated words (tuple of the word, 
                  starting location and orientation)
        """
        curr_location = location
        generated_words = []
        for _ in word:
            if orientation == "h":
                generated_words.append(self._find_longest_word(curr_location, "v"))
                curr_location = right(curr_location)
            else:
                generated_words.append(self._find_longest_word(curr_location, "h"))
                curr_location = below(curr_location)


        return generated_words

    def _find_longest_word(self, location: Tuple[int, int], orientation: str):
        """
        Given a location and orientation, find the longest
        word we can make. The word does not have to be valid.

        :param location: location of the starting letter
        :param orientation: orientation of the word
        :returns: Tuple of longest word, starting location and orientation
        """
        moving_direction = above if orientation == "v" else left
        word = self[location].letter

        start = location
        curr_location = moving_direction(location)
        while isinstance(self[curr_location], Letter):
            word = self[curr_location].letter + word
            start = curr_location
            curr_location = moving_direction(curr_location)

        moving_direction = below if orientation == "v" else right
        curr_location = moving_direction(location)
        while isinstance(self[curr_location], Letter):
            word += self[curr_location].letter
            curr_location = moving_direction(curr_location)

        if len(word) > 1:
            return word, start, orientation

        return "", start, orientation

    def _is_valid(self, word):
        return True

    def _get_score(self, word: str, location: Tuple[int, int], orientation: str):
        """get_score

        Given a word on a location on the board with a certain orientation
        (horizontal or vertical), calculate the score that playing the score
        yields us.

        the tricky part is when the word that we want to calculate the score of
        is extending another word and the letter which extends the word is sitting
        on a score multiplier, so this function must keep track of board bonuses
        that have been used before should not be used again.

        the way it does that is by checking if there is already a letter on the board
        if there's a letter that means the multiplier has been used before and will
        not contribute towards the score.

        :param word: the word we are placing
        :param location: location of the word
        :param orientation: h or v
        :returns: the score of placing that word
        """
        score = 0

        curr_location = location
        score_multiplier = 1
        for letter in Letter.from_str(word):
            letter_points = letter.point
            curr_location_occupied = isinstance(self[curr_location], Letter)
            loc_bonus = Board.BONUS.get(curr_location, "")
            if loc_bonus and not curr_location_occupied:
                if loc_bonus == Board.DOUBLE_LETTER:
                    letter_points *= 2
                elif loc_bonus == Board.TRIPLE_LETTER:
                    letter_points *= 3

                if loc_bonus == Board.DOUBLE_WORD:
                    score_multiplier *= 2
                elif loc_bonus == Board.TRIPLE_WORD:
                    score_multiplier *= 3
            score += letter_points

            if orientation == "v":
                curr_location = below(curr_location)
            elif orientation == "h":
                curr_location = right(curr_location)

        return score * score_multiplier
